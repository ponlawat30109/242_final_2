using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class PlayerController : MonoBehaviourPunCallbacks, IPunObservable, IDamagable
{
    public static PlayerController instance;

    public Animator animator;
    public Rigidbody2D rb;

    [SerializeField] GameObject arrow;
    [SerializeField] Transform shootpoint;
    public float speed;
    public int hp;
    // public bool isFlip;

    private Vector3 correctPosition;
    private Quaternion correctRotation;
    private int correctHP;
    // private bool correctFlip;

    private SpriteRenderer spriteRenderer;
    int playerIndex;
    string hero;
    string path;

    public event Action OnExploded;

    private void Awake()
    {
        instance = this;

        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (photonView.IsMine)
        {
            Movement();
            Fire();
            // AnimationSet();
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, correctPosition, 5f * Time.deltaTime);
            hp = correctHP;
            // isFlip = correctFlip;
        }
    }

    void Movement()
    {
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
        transform.position += movement * Time.deltaTime * speed;

        // if (Input.GetKeyDown(KeyCode.A))
        // {
        // isFlip = true;
        // spriteRenderer.flipX = true;
        // }

        // if (Input.GetKeyDown(KeyCode.D))
        // {
        // isFlip = false;
        // spriteRenderer.flipX = false;
        // }

        // spriteRenderer.flipX = isFlip;
    }

    void Fire()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // animator.SetBool("isAttack", true);
            FireRPC();
        }

        // if (Input.GetMouseButtonUp(0))
        // {
        //     animator.SetBool("isAttack", false);
        // }
    }

    [PunRPC]
    void FireRPC()
    {
        foreach (Player player in PhotonNetwork.PlayerList)
        {
            if (player == PhotonNetwork.LocalPlayer)
            {
                playerIndex = 0;
                path = "Prefabs/Arrow1";
            }
            else
            {
                playerIndex = 1;
                path = "Prefabs/Arrow2";
            }
        }
        PhotonNetwork.Instantiate(Path.Combine(path, "Arrow"), shootpoint.position, Quaternion.identity, 0);
    }

    public void TakeHit(int damage)
    {
        hp -= damage;
        if (hp > 0)
        {
            return;
        }

        // Destroy(gameObject);
        Explode();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
            stream.SendNext(hp);
            // stream.SendNext(isFlip);
        }
        else
        {
            correctPosition = (Vector3)stream.ReceiveNext();
            correctRotation = (Quaternion)stream.ReceiveNext();
            correctHP = (int)stream.ReceiveNext();
            // correctFlip = (bool)stream.ReceiveNext();
        }
    }

    public void Explode()
    {
        // foreach (Player player in PhotonNetwork.PlayerList)
        // {
        //     if (player == PhotonNetwork.LocalPlayer)
        //     {
        //         playerIndex = 0;
        //         Debug.Log(player + " : " + true);
        //     }
        //     else
        //     {
        //         playerIndex = 1;
        //         Debug.Log(player + " : " + false);
        //     }
        // }
        Destroy(gameObject);
        OnExploded?.Invoke();
    }
}
