using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomCanvas : MonoBehaviour
{
    private MenuManager _menuManager;
    [SerializeField] RoomLobbyManager _roomLobbyManager;
    [SerializeField] LeaveRoom _leaveRoom;

    public void FirstInitialize(MenuManager canvas)
    {
        _menuManager = canvas;
        _roomLobbyManager.FirstInitialize(canvas);
        _leaveRoom.FirstInitialize(canvas);
    }

    public void ShowCanvas() => gameObject.SetActive(true);

    public void HideCanvas() => gameObject.SetActive(false);
}
