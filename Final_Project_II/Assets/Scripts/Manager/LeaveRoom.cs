using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class LeaveRoom : MonoBehaviour
{
    public static LeaveRoom instance;

    private MenuManager _menuManager;
    [SerializeField] RoomLobbyManager _roomLobbyManager;

    private void Awake()
    {
        instance = this;
    }

    public void FirstInitialize(MenuManager canvas)
    {
        _menuManager = canvas;
        // _roomLobbyManager.FirstInitialize(canvas);
    }

    public void OnLeaveRoom()
    {
        PhotonNetwork.LeaveRoom(true);
        _menuManager.roomCanvas.HideCanvas();
    }
}
