using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RoomListing : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI roomname;
    // [SerializeField] InputField roomnameInput;

    public RoomInfo roomInfo { get; private set; }

    public void SetRoomInfo(RoomInfo roominfo)
    {
        roomInfo = roominfo;
        roomname.text = $"{roominfo.PlayerCount}/{roominfo.MaxPlayers} | {roominfo.Name}";
    }

    public void OnClickButton()
    {
        PhotonNetwork.JoinRoom(roomInfo.Name);
        // roomnameInput.text = roomInfo.Name;
        PhotonNetwork.NickName = "Player";
    }
}
