using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RoomManager : MonoBehaviourPunCallbacks
{
    public static RoomManager instance;

    [SerializeField] Button startButton;

    [SerializeField] PlayerListing playerListing;
    [SerializeField] Transform content;
    [SerializeField] TMP_Dropdown stageSelector;

    private List<PlayerListing> playerListings = new List<PlayerListing>();

    private MenuManager _menuManager;
    // private RoomCanvas _roomCanvas;

    public void FirstInitialize(MenuManager canvas)
    {
        _menuManager = canvas;
    }

    private void Awake()
    {
        instance = this;

        startButton.onClick.AddListener(OnStartGame);
    }

    private void Update()
    {
        startButton.gameObject.SetActive(PhotonNetwork.IsMasterClient);
        stageSelector.gameObject.SetActive(PhotonNetwork.IsMasterClient);
    }

    public void OnStartGame()
    {
        int selectStage = stageSelector.value;
        // rawImage.texture = stageImg[0];

        if (PhotonNetwork.IsMasterClient && PhotonNetwork.CurrentRoom.PlayerCount == PhotonNetwork.CurrentRoom.MaxPlayers)
        // if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel(selectStage + 1);
        }
    }

    public override void OnLeftRoom()
    {
        content.DestroyChildren();
    }

    public void GetCurrentPlayer()
    {
        if (!PhotonNetwork.IsConnected)
            return;
        if (PhotonNetwork.CurrentRoom == null || PhotonNetwork.CurrentRoom.Players == null)
            return;

        foreach (KeyValuePair<int, Player> playerInfo in PhotonNetwork.CurrentRoom.Players)
        {
            AddingPlayerList(playerInfo.Value);
        }
    }

    private void AddingPlayerList(Player player)
    {
        int index = playerListings.FindIndex(x => x.player == player);
        if (index != -1)
        {
            playerListings[index].SetPlayerInfo(player);
        }
        else
        {
            PlayerListing listing = Instantiate(playerListing, content);
            if (listing != null)
            {
                listing.SetPlayerInfo(player);
                playerListings.Add(listing);
            }
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        AddingPlayerList(newPlayer);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        int index = playerListings.FindIndex(x => x.player == otherPlayer);
        if (index != -1)
        {
            Destroy(playerListings[index].gameObject);
            playerListings.RemoveAt(index);
        }
    }
}
