using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;

public class ReturnToStartScene : MonoBehaviourPunCallbacks
{
    [SerializeField] TextMeshProUGUI text;
    private float timer = 5f;

    void Update()
    {
        if (timer >= 0)
        {
            timer -= Time.deltaTime;
            text.text = $"Return to Start Scene in {timer:0} Second";

            // PhotonNetwork.LoadLevel(0);
            // PhotonNetwork.LeaveRoom();
        }

        if (timer < 0)
        {
            text.text = "Press any key to return to menu";
            if (Input.anyKeyDown)
            {
                PhotonNetwork.LoadLevel(0);
            }
        }
    }

    // public override void OnLeftRoom()
    // {
    //     PhotonNetwork.LoadLevel(0);
    // }
}
