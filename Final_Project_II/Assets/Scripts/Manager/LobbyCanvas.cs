using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyCanvas : MonoBehaviour
{
    private MenuManager _menuManager;
    [SerializeField] RoomLobbyManager _roomLobbyManager;

    public void FirstInitialize(MenuManager canvas)
    {
        _menuManager = canvas;
        _roomLobbyManager.FirstInitialize(canvas);
    }

    public void ShowCanvas() => gameObject.SetActive(true);

    public void HideCanvas() => gameObject.SetActive(false);
}
