using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviourPunCallbacks, IPunObservable
{
    public static GameManager instance;
    [SerializeField] Transform[] spawnpoint;
    [SerializeField] TextMeshProUGUI hpText;
    [SerializeField] Image hpBar;

    GameObject ownerChar;
    PlayerController playerController;
    // Player[] playerList;
    private PhotonView _photonView;
    private int playerIndex = 0;
    string hero;
    string path;
    bool isEnd = false;
    PlayerController _player;

    // private List<PlayerList> playerList = new List<PlayerList>();

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;

        _photonView = GetComponent<PhotonView>();
        // playerList = PhotonNetwork.PlayerList;
        foreach (Player player in PhotonNetwork.PlayerList)
        {
            if (player == PhotonNetwork.LocalPlayer)
            {
                // playerIndex++;
                playerIndex = 0;
                Debug.Log(player + " : " + true);
                hero = "Hero1";
                path = "Prefabs/Hero1";
            }
            else
            {
                playerIndex = 1;
                Debug.Log(player + " : " + false);
                hero = "Hero2";
                path = "Prefabs/Hero2";
            }
        }

        ownerChar = PhotonNetwork.Instantiate(Path.Combine(path, hero), spawnpoint[playerIndex].position, Quaternion.identity, 0);
        _player = ownerChar.GetComponent<PlayerController>();
        _player.OnExploded += OnPlayerDie;
        // PhotonNetwork.LeaveRoom();
    }

    void Update()
    {
        // if (isEnd)
        // {
        //     StartCoroutine(EndScene());
        // }

        if (_player != null)
        {
            hpText.text = "HP : " + _player.hp.ToString();
            float remainHP = _player.hp/500;
            hpBar.fillAmount = remainHP;
        }
    }

    private void OnPlayerDie()
    {
        // isEnd = true;
        StartCoroutine(EndScene());
    }

    IEnumerator EndScene()
    {
        yield return new WaitForSeconds(0.1f);
        PhotonNetwork.LeaveRoom();
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        StartCoroutine(EndScene());
        // base.OnPlayerLeftRoom(otherPlayer);
    }

    public override void OnLeftRoom()
    {
        PhotonNetwork.LoadLevel(3);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        // if (stream.IsWriting)
        // {
        //     stream.SendNext(isEnd);
        // }
        // else
        // {
        //     isEnd = (bool)stream.ReceiveNext();
        // }
    }
}
