using System.Collections;
using System.Collections.Generic;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerListing : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI playerName;

    public Player player { get; private set; }

    public void SetPlayerInfo(Player _player)
    {
        player = _player;
        playerName.text = player.NickName;
    }
}
