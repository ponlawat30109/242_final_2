using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    [SerializeField] LobbyCanvas _lobbyCanvas;
    public LobbyCanvas lobbyCanvas { get { return _lobbyCanvas; } }
    [SerializeField] RoomCanvas _roomCanvas;
    public RoomCanvas roomCanvas { get { return _roomCanvas; } }

    private void Awake()
    {
        FirstInitialize();
    }

    private void FirstInitialize()
    {
        lobbyCanvas.FirstInitialize(this);
        roomCanvas.FirstInitialize(this);
    }
}
