using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RoomLobbyManager : MonoBehaviourPunCallbacks
{
    public static RoomLobbyManager instance;

    [SerializeField] Button connectButton;
    [SerializeField] TMP_InputField playerNameInput;
    [SerializeField] TMP_InputField roomnameInput;

    [SerializeField] RoomListing roomListing;
    [SerializeField] Transform content;

    // [SerializeField] Canvas roomCanvas;

    private List<RoomListing> roomListings = new List<RoomListing>();

    private MenuManager _menuManager;

    public void FirstInitialize(MenuManager canvas)
    {
        _menuManager = canvas;
    }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        connectButton.onClick.AddListener(OnConnectButton);
    }

    void OnConnectButton()
    {
        if (!PhotonNetwork.IsConnected)
            return;

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 2;
        roomOptions.IsOpen = true;
        roomOptions.IsVisible = true;

        // byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
        // byte[] key = Guid.NewGuid().ToByteArray();
        // string token = Convert.ToBase64String(time.Concat(key).ToArray());
        string roomname;

        if (string.IsNullOrEmpty(roomnameInput.text))
            // roomname = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            roomname = Guid.NewGuid().ToString();
        // roomname = token;
        else
            roomname = roomnameInput.text;

        PhotonNetwork.JoinOrCreateRoom(roomname, roomOptions, TypedLobby.Default);

        if (string.IsNullOrEmpty(playerNameInput.text))
            PhotonNetwork.NickName = "Player";
        else
            PhotonNetwork.NickName = playerNameInput.text;

        Debug.Log($"Player Name : {PhotonNetwork.LocalPlayer.NickName}");
    }

    public override void OnJoinedRoom()
    {
        _menuManager.roomCanvas.ShowCanvas();
        RoomManager.instance.GetCurrentPlayer();

        content.DestroyChildren();
        roomListings.Clear();
    }

    public override void OnCreatedRoom()
    {
        _menuManager.roomCanvas.ShowCanvas();
        // RoomManager.instance.GetCurrentPlayer();
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        foreach (RoomInfo info in roomList)
        {
            // Debug.Log("test");
            if (info.RemovedFromList)
            {
                int index = roomListings.FindIndex(x => x.roomInfo.Name == info.Name);
                if (index != -1)
                {
                    Destroy(roomListings[index].gameObject);
                    roomListings.RemoveAt(index);
                }
            }
            else
            {
                int index = roomListings.FindIndex(x => x.roomInfo.Name == info.Name);
                if (index == -1)
                {
                    RoomListing room = Instantiate(roomListing, content);
                    if (room != null)
                    {
                        room.SetRoomInfo(info);
                        roomListings.Add(room);
                    }
                }
                // else
                // {

                // }
            }
        }
    }
}
