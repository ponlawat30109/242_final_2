using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow2 : MonoBehaviour
{

    [SerializeField] private int damage;
    public float speed;
    [SerializeField] private Rigidbody2D rigidbody2D;

    // public void Init(Vector2 direction)
    // {
    //     Move(direction);
    // }

    private void Awake()
    {
        Destroy(gameObject, 2f);
    }

    // private void Move(Vector2 direction)
    // {
    //     rigidbody2D.velocity = direction * speed;
    // }

    void Update()
    {
        // this.transform.Translate(Vector3.forward * speed * Time.deltaTime);
        // rigidbody2D.velocity = transform.TransformDirection(new Vector3(speed, 0, 0));
        rigidbody2D.velocity = transform.TransformDirection(Vector3.left * speed);

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
            Destroy(gameObject);

        var target = other.gameObject.GetComponent<IDamagable>();
        target?.TakeHit(damage);
    }
}
