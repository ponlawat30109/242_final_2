﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playermovement : MonoBehaviour
{
    public Animator animator;
    public Rigidbody2D rb;
    public float Lerpconstant;
    public float speed;

// Update is called once per frame
        void FixedUpdate()
        {
            float h = Input.GetAxisRaw("Horizontal");
            Vector2 movement = new Vector2(h, rb.velocity.y);
            rb.velocity = Vector2.Lerp(rb.velocity, speed * movement , Lerpconstant);
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                animator.SetBool("Run", true);
            }

            if (Input.GetKeyUp(KeyCode.A))
            {
                animator.SetBool("Run", false);
            }
        }
    }